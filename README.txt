FEED Module 
Developed by greenman  
	Drupal: The Greenman
	Greenman@the-organization.com



The objective of this module is to create a system for themeing feeds. This involves creating 
some themeable funcitons for actually outputting feeds and then using xslt for themeing the feeds themselves. 

We provide a basic XSL theme, based on the BBC theme that will render your RSS feeds in a pretty way. 
This stylesheet also uses a little bit of javascript to render HTML nodes that have been escaped. 


STYLING YOUR FEEDS
The simplest way to change your feeds is to just copy feed.css to your theme directory and override the styles.

To get a little deeper into the styling, you can create a template overrride and modify the XSL. There are
some issues with making your own XSL stylesheet (see the notes inside the stylesheet), so you may need to 
do some experimenting.   

VIEWS RSS 
We replace the default feed handler for node and taxonomy terms, and provide a function 
that can be used to overrride the feed function for the views_rss module. You will need to 
create a function in your own template.php file to be make this work. 

It should look something like this : 

	function phptemplate_views_rss_feed($view, $nodes){
		feed_views_rss_feed($view, $nodes); 
	} 







