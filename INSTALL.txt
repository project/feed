INSTALLATION

You should need to do little more than copy all the files into your modules directory. 

The .install file will set the weight of your module to 1 so that it can override the rss
callbacks for node and taxonomy. If this does not work, you may need to run the SQL from 
the .install file manually. 

Copy feed.css to your theme directory to change the visual styles. 
Override the functions in feed.theme to modify the HTML styled output. 
