<? 
/**
 This is an XSL style sheet. But, it is passed through a php interpreter. 
 So, there may be a few things that break if you are not careful. 
 The first issue is the fact that your php installation may be allowing the use of short php tags: 
 "<?" rather than "<?php". Since an xml file will use "<?" to mark processing instructions, we 
 will need to "print" any lines that may contain them

	This stylesheet is based on the BBC feed xslt. 
    Since Drupal escapes it's RSS content, which is probably a good idea since we cannot guarantee valid xhtml in the content yet, 
    there is a need to do some "unescaping". In some viewers, this should be handled by using "disable-output-escaping" (DOE),
    but many renderers don't do this, so we have taken another route and added some javascript. 
    
    We are using xsl_mop-up.js from  Sean M. Burke (sburke@cpan.org) to do some postprocessing. This script tells the browser
    to re-render some of the content, turning it from the source into the nice pretty output. 
    Some basic notes on using this mopup script: 
    	- we have an onload event that does the mopup
    	  - there is a test div with an id of "cometestme" which is checked, to see if the renderer supports DOE
    	- all elements that should be decoded need a 'name="decodeme"' attribute.   
    
   I have placed the php statements into the variable definitions at the beginning of the file. It probably makes sense to keep 
   all the php stuff in one place, and use xsl syntax wherever you can - it should make validating and debugging your stylesheets 
   a little easier.
   
  
*/

print '<?xml version="1.0" encoding="ISO-8859-1" ?>';
?>
<!DOCTYPE xsl:stylesheet  [
	<!ENTITY nbsp   "&#160;">
	<!ENTITY copy   "&#169;">
	<!ENTITY reg    "&#174;">
	<!ENTITY trade  "&#8482;">
	<!ENTITY mdash  "&#8212;">
	<!ENTITY ldquo  "&#8220;">
	<!ENTITY rdquo  "&#8221;"> 
	<!ENTITY pound  "&#163;">
	<!ENTITY yen    "&#165;">
	<!ENTITY euro   "&#8364;">
]>
<xsl:stylesheet version="1.1" xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:dc="http://purl.org/dc/elements/1.1/">
  <xsl:output method="html" encoding="iso-8859-1" />

   <xsl:variable name="title" select="/rss/channel/title"/>
   <xsl:variable name="site_name"><?php print  variable_get('site_name', 'Drupal') ?></xsl:variable>	
   <xsl:variable name="logo"><?php print theme_get_setting('logo') ?></xsl:variable>	   
   <xsl:variable name="base_url"><?php global $base_url; print $base_url; ?></xsl:variable>
   <xsl:variable name="base_path"><?php print base_path() ?></xsl:variable>
   <xsl:variable name="theme_path">/<?php print path_to_theme() ?>/</xsl:variable>
   <xsl:variable name="module_path">/<?php print drupal_get_path('module','feed') ?>/</xsl:variable>
   <xsl:variable name="feed_path"><?php print urldecode($_GET[feedpath]); ?></xsl:variable>
   

   	

  <xsl:template match="/">

    <html xmlns="http://www.w3.org/1999/xhtml">
      <head>
        <title><xsl:value-of select="$title"/> XML Feed</title>
          <link rel="stylesheet" href="{$module_path}feed.css" type="text/css"/>
          <link rel="stylesheet" href="{$theme_path}feed.css" type="text/css"/>
		  
      	<script type="text/javascript"  src="{$module_path}xsl_mop-up.js"  ></script>
        <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
      </head>
      
    <xsl:apply-templates select="rss/channel"/>
    </html>
  </xsl:template>

<!-- Channel Template --> 

    <xsl:template match="channel">
    <html xmlns="http://www.w3.org/1999/xhtml">
    <body onload="go_decoding();">
    	<!-- set up our DOEY check --> 
		<div id="cometestme" style="display:none;"><xsl:text disable-output-escaping="yes" >&amp;amp;</xsl:text></div>
		<div class="feed">   
			<div class="header">
				<xsl:choose>
					<xsl:when test="string($logo)">
						<div class="logo"><a href="{$base_path}" title="Home"><img src="{$logo}" alt="Home" border="0" /></a></div>
					</xsl:when>		
		   	    </xsl:choose>
				<div class="site-name"><a href="{$base_path}" title="Home"><h1> RSS Feed For: <xsl:value-of select="$title"/></h1></a></div>
       		</div>
 	   		<br clear="all" />	
      		<div class="elements">
          		<xsl:apply-templates select="item"/>
      		</div>    
			<div class="sidebar">   	
				<div class="rss_logo">
					<a href="{$base_url}{$base_path}?q={$feed_path}" class="item"><img src="{$module_path}128px-Feed-icon.svg.png" alt="RSS icon"  border="0" /></a>
				</div>    
				<div class="subscribe">
					<h2>Subscribe to this feed</h2>
					<p>You can subscribe to this RSS feed in a number of ways, including the following:</p>
					<ul>
						<li>Drag the orange RSS button into your Feed Reader</li>
						<li>Drag the URL of the RSS feed into your Feed Reader</li>
						<li>Cut and paste the URL of the RSS feed into your Feed Reader</li>
					</ul>
	    		</div>
		 		<div class="explain">
      				<h2>What are RSS Feeds</h2> 
		      		<p>This is an RSS feed from the <xsl:value-of select="$site_name" /> website. 
			  	 		RSS feeds allow you to stay up to date with the latest news and features you want from  <xsl:value-of select="$site_name" />.
			  		</p>
		      		<p>You will need to have a feed reader of some kind to make use of this feed. 
			    		(For an extensive list of readers, have a look at <a href="http://www.newsonfeeds.com/faq/aggregators"> newsonfeeds.com</a>)			  
			  		</p>
				</div>  	  
    		</div>
    		<div class="footer">
			</div>
    	</div>
    </body>
  </html>
    </xsl:template>
  
  <!--  This template is applied to each of the items in the RSS feed -->
  <xsl:template match="item">
  <div class="item">
		<h3><a href="{link}" class="item">
	          <xsl:value-of select="title"/>
    	    </a>
		</h3>        
		<div name="decodeme" class="content">
	        <xsl:value-of select="description" disable-output-escaping="yes"/>
        </div>
  </div>
  </xsl:template>

</xsl:stylesheet>